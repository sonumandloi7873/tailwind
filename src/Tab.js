import { View, Text } from 'react-native'
import React from 'react'
import { createBottomTabNavigator } from '@react-navigation/bottom-tabs'
import About from './Component/About';
import Home from './Home';

const Tab = createBottomTabNavigator();

const TabNavigation = () => {
  return (
    <Tab.Navigator>
      <Tab.Screen name='About' component={About} />
      <Tab.Screen name='Home' component={Home} />
    </Tab.Navigator>
  )
}

export default TabNavigation