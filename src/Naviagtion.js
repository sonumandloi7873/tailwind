import { View, Text } from 'react-native'
import React from 'react'
import {NavigationContainer} from '@react-navigation/native'
import { createNativeStackNavigator } from '@react-navigation/native-stack'
import Home from './Home'
import About from './Component/About'
import TabNavigation from './Tab'


const Stack = createNativeStackNavigator()

const Naviagtion = () => {
  return (
    <NavigationContainer>
        <Stack.Navigator>
        <Stack.Screen name="Tab" component={TabNavigation} />

        </Stack.Navigator>
    </NavigationContainer>
  )
}

export default Naviagtion