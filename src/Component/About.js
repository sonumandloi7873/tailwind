import { SafeAreaView, StyleSheet, Text, View } from 'react-native'
import React from 'react'

const About = () => {
  return (
    <SafeAreaView className='border-2 bg-pink-300'>
      <Text className={`${styles.text} ${styles.fontWeight}`}>About</Text>
    </SafeAreaView>
  )
}

export default About

const styles = {
  text : "text-3xl text-red-500",
  fontWeight : "font-bold"

}

const Container = {

}