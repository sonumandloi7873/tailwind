import { Button, StyleSheet, Text, TouchableOpacity, View } from 'react-native'
import React from 'react'
import { useNavigation } from '@react-navigation/native'

const Home = () => {
  const navigation = useNavigation()
  return (
    <View>
      <Text className="text-5xl">Home</Text>
      {/* <Button title='press' onPress={()=>navigation.navigate('About')} /> */}
    </View>
  )
}

export default Home

const styles = StyleSheet.create({})